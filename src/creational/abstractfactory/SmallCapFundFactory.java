package creational.abstractfactory;

public class SmallCapFundFactory extends CapFundFactory {

    public SmallCapFund getFund(String s) {
        if("KRISHNA".equals(s)) return new KrishnaFund();
        if("MURTHY".equals(s)) return new MurthyFund();
        if("PINE".equals(s)) return new PineFund();
        throw new RuntimeException("NO SUCH FUND");
    }
}
