package creational.abstractfactory;

public abstract class CapFundFactory {
    abstract Fund getFund(String s);
}
