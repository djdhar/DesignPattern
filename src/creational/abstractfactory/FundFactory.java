package creational.abstractfactory;

public class FundFactory {

    public CapFundFactory getFundFactory(String s) {
        if("LARGE".equals(s)) return new LargeCapFundFactory();
        if("MID".equals(s)) return new MidCapFundFactory();
        if("SMALL".equals(s)) return new SmallCapFundFactory();
        throw new RuntimeException("NO SUCH FUND FACTORY");
    }
}
