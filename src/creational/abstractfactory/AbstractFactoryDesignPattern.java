package creational.abstractfactory;

public class AbstractFactoryDesignPattern {
    public static void driver() {
        FundFactory fundFactory = new FundFactory();
        fundFactory.getFundFactory("SMALL").getFund("KRISHNA").getFundName();
        fundFactory.getFundFactory("SMALL").getFund("PINE").getFundName();
        fundFactory.getFundFactory("SMALL").getFund("MURTHY").getFundName();
        fundFactory.getFundFactory("MID").getFund("LUX").getFundName();
        fundFactory.getFundFactory("MID").getFund("COLGATE").getFundName();
        fundFactory.getFundFactory("MID").getFund("AMBUJA").getFundName();
        fundFactory.getFundFactory("LARGE").getFund("ADANI").getFundName();
        fundFactory.getFundFactory("LARGE").getFund("RELIANCE").getFundName();
        fundFactory.getFundFactory("LARGE").getFund("TCS").getFundName();
        try {
            fundFactory.getFundFactory("MID").getFund("INVALID").getFundName();
        } catch (Exception e) {
            System.out.println(e);
        }
        try {
            fundFactory.getFundFactory("INVALID").getFund("KRISHNA").getFundName();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
