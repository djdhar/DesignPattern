package creational.abstractfactory;

public class MidCapFundFactory extends CapFundFactory {

    public MidCapFund getFund(String s) {
        if("COLGATE".equals(s)) return new ColgateFund();
        if("AMBUJA".equals(s)) return new AmbujaFund();
        if("LUX".equals(s)) return new LuxFund();
        throw new RuntimeException("NO SUCH FUND");
    }
}
