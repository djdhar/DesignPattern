package creational.abstractfactory;

abstract class Fund {
    public String getFundName() {
        System.out.println(this.getClass().getSimpleName());
        return this.getClass().getSimpleName();
    };
}
