package creational.abstractfactory;

public class LargeCapFundFactory extends CapFundFactory{
    public LargeCapFund getFund(String s) {
        if("ADANI".equals(s)) return new AdaniFund();
        if("RELIANCE".equals(s)) return new RelianceFund();
        if("TCS".equals(s)) return new TCSFund();
        throw new RuntimeException("NO SUCH FUND");
    }
}
