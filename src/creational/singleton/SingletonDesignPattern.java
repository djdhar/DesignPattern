package creational.singleton;

public class SingletonDesignPattern {

    public static void driver() {
        House.getInstance();
        House.getInstance();
        House.getInstance();
        House.getInstance();
        House.getInstance();
    }
}
