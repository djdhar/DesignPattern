package creational.singleton;

import java.util.Objects;
import java.util.UUID;

public class House {

    String houseUUID = UUID.randomUUID().toString();
    private static House house;

    public static House getInstance() {

//        House house = new House();
//        house.toString();
//        return house;

        if(Objects.isNull(house)) {
            house = new House();
            house.printName();
            return house;
        }
        house.printName();
        return house;

    }

    public void printName() {
        String houseString = houseString();
        System.out.println(houseString);
    }

    public String houseString() {
        return "House{" +
                "houseUUID='" + houseUUID + '\'' +
                '}';
    }
}
