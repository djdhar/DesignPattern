package creational.prototype;

public class Employee implements ProtoType {

    String empId;
    String empName;
    Double empSalary;

    public Employee(String empId, String empName, Double empSalary) {
        this.empId = empId;
        this.empName = empName;
        this.empSalary = empSalary;
    }

    @Override
    public Employee getClone() {
        return new Employee(empId, empName, empSalary);
    }

    @Override
    public String toString() {
        System.out.println(this.hashCode());
        return "Employee{" +
                "empId='" + empId + '\'' +
                ", empName='" + empName + '\'' +
                ", empSalary=" + empSalary +
                '}';
    }
}
