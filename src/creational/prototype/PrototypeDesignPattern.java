package creational.prototype;

public class PrototypeDesignPattern {
    public static void driver() {
        Employee employee = new Employee("id", "name", 500.0);
        Employee employeeAlias = employee;
        System.out.println("Employee : " + employee);
        System.out.println("Employee : " + employeeAlias);
        Employee employeeClone = employee.getClone();
        System.out.println("Cloned employee : " + employeeClone);
    }
}
