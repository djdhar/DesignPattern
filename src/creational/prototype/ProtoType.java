package creational.prototype;

public interface ProtoType {
    ProtoType getClone();
}
