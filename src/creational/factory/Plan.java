package creational.factory;

abstract class Plan {
    Double rate;
    abstract Double getRate();

    public void calculateBill(int unit) {
        System.out.println(unit*getRate());
    }

}
