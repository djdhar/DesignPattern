package creational.factory;

public class DomesticPlan extends Plan {
    @Override
    Double getRate() {
        return 500.0;
    }
}
