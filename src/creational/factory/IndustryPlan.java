package creational.factory;

public class IndustryPlan extends Plan {
    @Override
    Double getRate() {
        return 800.0;
    }
}
