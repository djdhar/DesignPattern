package creational.factory;

public class FactoryDesignPattern {

    public static void drive() {
        ElectricityBillPlanFactory electricityBillPlanFactory = new ElectricityBillPlanFactory();
        electricityBillPlanFactory.getElectricityBillPlan("DOMESTIC").calculateBill(500);
        electricityBillPlanFactory.getElectricityBillPlan("INDUSTRY").calculateBill(500);
        electricityBillPlanFactory.getElectricityBillPlan("INSTITUTE").calculateBill(500);
        electricityBillPlanFactory.getElectricityBillPlan("NOTHING").calculateBill(500);
    }
}
