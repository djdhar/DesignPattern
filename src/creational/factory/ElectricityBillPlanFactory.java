package creational.factory;

public class ElectricityBillPlanFactory {

    public Plan getElectricityBillPlan(String plan) {
        if("DOMESTIC".equals(plan)) {
            return new DomesticPlan();
        }
        if("INDUSTRY".equals(plan)) {
            return new IndustryPlan();
        }
        if("INSTITUTE".equals(plan)) {
            return new InstitutePlan();
        }
        throw new RuntimeException("Not allowed among plans");
    }
}
