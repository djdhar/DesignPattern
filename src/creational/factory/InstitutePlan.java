package creational.factory;

public class InstitutePlan extends Plan {
    @Override
    Double getRate() {
        return 800.0;
    }
}
