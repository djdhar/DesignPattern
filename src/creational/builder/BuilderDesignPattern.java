package creational.builder;

public class BuilderDesignPattern {
    public static void driver() {
        CDBuilder cdBuilder = new CDBuilder();
        cdBuilder.addMacCD().addMacCD().addWindowsCD().addWindowsCD().addLinuxCD().addWindowsCD().build();
    }
}
