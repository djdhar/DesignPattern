package creational.builder;

public class LinuxCD extends CD {
    @Override
    public String pack() {
        return "Linux CD";
    }

    @Override
    public double price() {
        return 0.0;
    }
}
