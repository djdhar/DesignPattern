package creational.builder;

public class MacCD extends CD{
    @Override
    public String pack() {
        return "Mac CD";
    }

    @Override
    public double price() {
        return 50000.0;
    }
}
