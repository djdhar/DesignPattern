package creational.builder;

public class CDBuilder {

    CDPacker cdPacker = new CDPacker();
    public CDBuilder addWindowsCD() {
        cdPacker.addCD(new WindowsCD());
        return this;
    }

    public CDBuilder addLinuxCD() {
        cdPacker.addCD(new LinuxCD());
        return this;
    }

    public CDBuilder addMacCD() {
        cdPacker.addCD(new MacCD());
        return this;
    }

    public CDPacker build() {
        System.out.println(cdPacker);
        return cdPacker;
    }
}
