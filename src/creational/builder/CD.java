package creational.builder;

abstract class CD implements Packing {

    @Override
    public String toString() {
        return "Item : " + this.pack() + ", Price : " + this.price();
    }
}
