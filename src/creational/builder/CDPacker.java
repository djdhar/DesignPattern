package creational.builder;

import java.util.ArrayList;
import java.util.List;

public class CDPacker {
    public List<CD> cdList = new ArrayList<>();

    public void addCD(CD cd) {
        cdList.add(cd);
    }

    public void showCDs() {
        System.out.println(cdList);
    }

    @Override
    public String toString() {
        return "CDPacker{" +
                "cdList=" + cdList +
                '}';
    }
}
