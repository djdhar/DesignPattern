package creational.builder;

public class WindowsCD extends CD{
    @Override
    public String pack() {
        return "Windows CD";
    }

    @Override
    public double price() {
        return 500.0;
    }
}
