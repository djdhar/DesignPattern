import behavioral.modernobserver.ModernObserverDesignPattern;
import behavioral.observer.ObserverDesignPattern;

public class Main {
    public static void main(String[] args) throws InterruptedException {
//        FactoryDesignPattern.drive();
//        AbstractFactoryDesignPattern.driver();
//        SingletonDesignPattern.driver();
//        PrototypeDesignPattern.driver();
//        BuilderDesignPattern.driver();
//        ObjectPoolDesignPattern.driver();
//        AdapterDesignPattern.driver();
//          BridgeDesignPattern.driver();
//        CompositeDesignPattern.driver();
//        DecoratorDesignPattern.driver();
//        FacadeDesignPattern.driver();
//        ObserverDesignPattern.driver();
        ModernObserverDesignPattern.driver();
    }
}