package behavioral.observer;

import java.util.Observable;
import java.util.UUID;

public class MyObservable extends Observable implements Runnable{
    @Override
    public void run() {

        while (true) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            String resp = UUID.randomUUID().toString();
            System.out.println("State changed with resp : "+ resp);
            setChanged();
            notifyObservers(resp);
        }
    }
}
