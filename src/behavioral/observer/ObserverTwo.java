package behavioral.observer;

import java.util.Observable;
import java.util.Observer;

public class ObserverTwo implements Observer {
    @Override
    public void update(Observable o, Object arg) {
        if(arg instanceof String) {
            System.out.println("\nReceived Response by Observer 2: " + arg );
        }
    }
}
