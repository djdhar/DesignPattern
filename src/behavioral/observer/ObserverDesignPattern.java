package behavioral.observer;

public class ObserverDesignPattern {

    public static void driver() {
        MyObservable myObservable = new MyObservable();
        myObservable.addObserver(new ObserverOne());
        myObservable.addObserver(new ObserverTwo());
        Thread t1 = new Thread(myObservable);
        t1.start();
    }

}
