package behavioral.modernobserver;

public class ModernObserverDesignPattern {

    public static void driver() {
        MyObservable myObservable = new MyObservable();
        myObservable.subscribe(new ObserverOne());
        myObservable.subscribe(new ObserverTwo());
        Thread t1 = new Thread(myObservable);
        t1.start();
    }

}
