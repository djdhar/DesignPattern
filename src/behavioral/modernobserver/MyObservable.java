package behavioral.modernobserver;

import java.util.UUID;
import java.util.concurrent.SubmissionPublisher;

public class MyObservable extends SubmissionPublisher<String> implements Runnable {

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            String resp = UUID.randomUUID().toString();
            System.out.println("State changed with resp : " + resp);
            submit(resp);
        }
    }
}
