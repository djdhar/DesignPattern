package behavioral.modernobserver;

import java.util.concurrent.Flow.Subscription;

public class ObserverOne implements java.util.concurrent.Flow.Subscriber<String> {
    private Subscription subscription;
    private int counter = 0;

    @Override
    public void onSubscribe(Subscription subscription) {
        this.subscription = subscription;
        subscription.request(1); // request the first item
    }

    @Override
    public void onNext(String item) {
        System.out.println("Received Response by Observer 1: " + item);
        counter++;
        subscription.request(1); // request the next item
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println("An error occurred: " + throwable.getMessage());
    }

    @Override
    public void onComplete() {
        System.out.println("Completed receiving " + counter + " items");
    }
}

