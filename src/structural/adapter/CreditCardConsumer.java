package structural.adapter;

public interface CreditCardConsumer {
    public void collectBankDetails(String bankName, String accountHolderName, Long accountNumber);
    public String issueCreditCard();
}
