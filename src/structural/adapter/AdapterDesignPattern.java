package structural.adapter;

public class AdapterDesignPattern {
    public static void driver() {
        CreditCardConsumer creditCardConsumer = new BankCustomer();
        creditCardConsumer.collectBankDetails("myBankName", "myAccountHolderName", 679486984L);
        System.out.println(creditCardConsumer.issueCreditCard());
    }
}
