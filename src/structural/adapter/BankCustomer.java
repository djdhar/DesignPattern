package structural.adapter;

import java.util.UUID;

public class BankCustomer extends BankDetails implements CreditCardConsumer {

    @Override
    public void collectBankDetails(String bankName, String accountHolderName, Long accountNumber) {
        this.setBankName(bankName);
        this.setAccNumber(accountNumber);
        this.setAccHolderName(accountHolderName);
    }

    @Override
    public String issueCreditCard() {
        return "CreditCard {" +
                    "BankName: " +  getBankName() + ", " +
                    "AccountHolderName: " + getAccHolderName() + ", " +
                    "AccountNumber: " + getAccNumber() + ", " +
                    "CreditCardNumber: " + UUID.randomUUID() +
                "}";
    }
}
