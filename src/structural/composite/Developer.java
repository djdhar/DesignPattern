package structural.composite;

public class Developer implements Employee {

    String name;
    String position;

    public Developer(String name, String position) {
        this.name = name;
        this.position = position;
    }

    @Override
    public void showEmployeeDetails() {
        System.out.println("::D::----------------------------");
        System.out.println(this);
        System.out.println("::D::----------------------------");
    }

    @Override
    public String toString() {
        return "Developer{" +
                "name='" + name + '\'' +
                ", position='" + position + '\'' +
                '}';
    }
}
