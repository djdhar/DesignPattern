package structural.composite;


import java.util.ArrayList;
import java.util.List;

public class CompanyDirectory implements Employee {

    private List<Employee> employeeList = new ArrayList<>();
    @Override
    public void showEmployeeDetails() {
        System.out.println("::CD::----------------------------");
        for(Employee employee: employeeList) {
            employee.showEmployeeDetails();
        }
        System.out.println("::CD::----------------------------");
    }

    public void addEmployee(Employee employee) {
        employeeList.add(employee);
    }

    private void removeEmployee(Employee employee) {
        employeeList.remove(employee);
    }
}
