package structural.composite;

public class CompositeDesignPattern {
    public static void driver() {

        Developer developer1 = new Developer("Dev1", "dev");
        Developer developer2 = new Developer("Dev2", "dev");

        CompanyDirectory companyDirectory1 = new CompanyDirectory();
        companyDirectory1.addEmployee(developer1);
        companyDirectory1.addEmployee(developer2);
        System.out.println("---SED---");
        companyDirectory1.showEmployeeDetails();
        System.out.println("---SED---");

        Manager manager1 = new Manager("Man1", "man");
        Manager manager2 = new Manager("Man2", "man");

        CompanyDirectory companyDirectory2 = new CompanyDirectory();
        companyDirectory2.addEmployee(manager1);
        companyDirectory2.addEmployee(manager2);
        System.out.println("---SED---");
        companyDirectory2.showEmployeeDetails();
        System.out.println("---SED---");

        CompanyDirectory companyDirectory = new CompanyDirectory();
        companyDirectory.addEmployee(companyDirectory1);
        companyDirectory.addEmployee(companyDirectory2);
        System.out.println("---SED---");
        companyDirectory.showEmployeeDetails();
        System.out.println("---SED---");
    }
}
