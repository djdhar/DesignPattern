package structural.composite;

public class Manager implements Employee {
    String name;
    String position;

    public Manager(String name, String position) {
        this.name = name;
        this.position = position;
    }

    @Override
    public void showEmployeeDetails() {
        System.out.println("::M::----------------------------");
        System.out.println(this);
        System.out.println("::M::----------------------------");
    }

    @Override
    public String toString() {
        return "Manager{" +
                "name='" + name + '\'' +
                ", position='" + position + '\'' +
                '}';
    }
}
