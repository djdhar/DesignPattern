package structural.decorator;

public interface Food {
    public String prepareFood();
    public Double getPrice();
}
