package structural.decorator;

public class ChineseFood extends FoodDecorator {
    public ChineseFood(Food newFood) {
        super(newFood);
    }

    public String prepareFood() {
        return super.prepareFood() + "Prepared Chinese Food!\n";
    }
    public Double getPrice() {
        return super.getPrice() + 500.0;
    }
}
