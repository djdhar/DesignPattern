package structural.decorator;

public class DecoratorDesignPattern {

    public static void driver() {
        Food customFood1 = new ChineseFood(new NonVegFood(new VegFood()));
        Food customFood2 = new NonVegFood(new VegFood());
        Food customFood3 = new ChineseFood(new VegFood());
        System.out.print(customFood1.prepareFood());
        System.out.println(customFood1.getPrice());
        System.out.println();
        System.out.print(customFood2.prepareFood());
        System.out.println(customFood2.getPrice());
        System.out.println();
        System.out.print(customFood3.prepareFood());
        System.out.println(customFood3.getPrice());
        System.out.println();
    }

}
