package structural.decorator;

abstract class FoodDecorator implements Food {

    private Food newFood;

    public FoodDecorator(Food newFood) {
        this.newFood = newFood;
    }

    public String prepareFood() {
        return newFood.prepareFood();
    }
    public Double getPrice() {
        return newFood.getPrice();
    }
}
