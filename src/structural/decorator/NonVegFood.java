package structural.decorator;

public class NonVegFood extends FoodDecorator{
    public NonVegFood(Food newFood) {
        super(newFood);
    }

    public String prepareFood() {
        return super.prepareFood() + "Prepared Non Veg Food!\n";
    }
    public Double getPrice() {
        return super.getPrice() + 400.0;
    }
}
