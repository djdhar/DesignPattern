package structural.decorator;

import java.util.Objects;

public class VegFood implements Food{

    @Override
    public String prepareFood() {
        return "Prepared Veg Food!\n";
    }

    @Override
    public Double getPrice() {
        return 80.0;
    }
}
