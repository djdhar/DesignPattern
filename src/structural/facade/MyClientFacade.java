package structural.facade;

import java.time.Instant;
import java.util.Objects;

public class MyClientFacade {
    String cachedInformation = null;
    MyClient myClient = null;

    Instant lastFetched = null;

    String fetchInformation() {
//        if(Objects.nonNull(lastFetched)) {
//            System.out.println(Instant.now().toEpochMilli()-lastFetched.toEpochMilli());
//        }
            if(Objects.isNull(cachedInformation) || Objects.isNull(lastFetched) || Instant.now().toEpochMilli()-lastFetched.toEpochMilli()>=2000) {
            System.out.println("Information Fetched");
            lastFetched = Instant.now();
            return cachedInformation = getClient().getInformation();
        } else {
            return cachedInformation;
        }
    }

    MyClient getClient() {
        if(Objects.isNull(myClient)) {return myClient = new MyClient();}
        else return myClient;
    }


}
