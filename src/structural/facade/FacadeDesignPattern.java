package structural.facade;

public class FacadeDesignPattern {
    public static void driver() throws InterruptedException {
        MyClientFacade facade = new MyClientFacade();
        while (true) {
            System.out.println(facade.fetchInformation());
            Thread.sleep(500);
        }
    }
}
