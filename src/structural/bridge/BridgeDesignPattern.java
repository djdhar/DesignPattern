package structural.bridge;

public class BridgeDesignPattern {
    public static void driver() {
        Vehicle bike = new Bike(new ProductionWorkShop(), new AssembleWorkshop());
        Vehicle car = new Car(new ProductionWorkShop(), new AssembleWorkshop());
        bike.manufacture();
        car.manufacture();
    }
}
