package structural.bridge;

public interface Workshop {
    public void work();
}
