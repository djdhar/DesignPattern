package structural.bridge;

public class ProductionWorkShop implements Workshop {
    @Override
    public void work() {
        System.out.println("Produced !");
    }
}
