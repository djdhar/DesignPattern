# Design Patterns

## Creational Design Pattern
Creational Design Patterns are concerned with the process of object creation.

### Abstract Factory Design Pattern
This pattern work around a super-factory which creates other factories.

### Factory Design Pattern
This pattern creates object without exposing the creation logic to the client and refer to newly created object using a common interface.
### Builder Design Pattern
This pattern builds a complex object using simple objects and using a step-by-step approach.

### Object Pool Design Pattern
This pattern is used to reuse the object that are expensive to create
### ProtoType Design Pattern
This pattern creates new objects by copying existing ones.
### Singleton Design Pattern
This pattern ensures that only one instance of a class is created and provides a global point of access to it.

## Structural Design Pattern
Structural Design Patterns are concerned with object composition.



### Adapter Design Pattern
This pattern works as a bridge between two incompatible interfaces

### Bridge Design Pattern
This pattern decouples an abstraction from its implementation so that the two can vary independently.

### Composite Design Pattern
This pattern composes objects into tree structures to represent part-whole hierarchies.

This pattern attaches additional responsibilities to an object dynamically without altering its structure.

### Facade Design Pattern
This pattern provides a unified interface to a set of interfaces in a subsystem.


## Behavioral Design Pattern
Behavioral Design Patterns are concerned with communication between objects.

### Observer Design Pattern
This pattern defines a one-to-many dependency between objects so that when one object changes state, all its dependents are notified and updated automatically.
#### * Using deprecated Observer/Observable Class
#### * Using SubmissionPublisher and Flow.Subscriber Class